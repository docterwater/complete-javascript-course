/*****************************
* CODING CHALLENGE 1
*/

/* Mark and John are trying to compare their BMI (Body Mass Index), which is calculated using the formula: BMI = mass / height^2 = mass / (height * height). (mass in kg and height in meter).

1. Store Mark's and John's mass and height in variables
2. Calculate both their BMIs
3. Create a boolean variable containing information about whether Mark has a higher BMI than John.
4. Print a string to the console containing the variable from step 3. (Something like "Is Mark's BMI higher than John's? true"). 

GOOD LUCK 😀 
*/

var MarkHeight=1.69,JohnHeight=1.95,MarkBMI,JohnBMI,MarkMass=69,JohnMass=92;
MarkBMI = MarkMass/(MarkHeight*MarkHeight);
JohnBMI = JohnMass/(JohnHeight*JohnHeight);
MarkHeigher = MarkBMI > JohnBMI;

console.log(MarkBMI,JohnBMI);
console.log('Is Mark\'s BMI higher than John\'s?'+MarkHeigher);

/*****************************
* CODING CHALLENGE 2
*/

/*
John and Mike both play basketball in different teams. In the latest 3 games, John's team scored 89, 120 and 103 points, while Mike's team scored 116, 94 and 123 points.

1. Calculate the average score for each team
2. Decide which teams wins in average (highest average score), and print the winner to the console. Also include the average score in the output.
3. Then change the scores to show different winners. Don't forget to take into account there might be a draw (the same average score)

4. EXTRA: Mary also plays basketball, and her team scored 97, 134 and 105 points. Like before, log the average winner to the console. HINT: you will need the && operator to take the decision. If you can't solve this one, just watch the solution, it's no problem :)
5. Like before, change the scores to generate different winners, keeping in mind there might be draws.

GOOD LUCK 😀
*/


var MikeAverage =  (189 + 120 + 103) / 3;
var JohnAverage = (129 + 94 + 123) / 3;
var MaryAverage = (97 + 134 + 105) / 3;
console.log(MikeAverage,JohnAverage,MaryAverage);

if(MikeAverage>JohnAverage && MikeAverage>MaryAverage){
    console.log('Mike\'s average score is ' + MikeAverage +', he\'s the winner' );
}else if(MikeAverage === JohnAverage || MikeAverage === MaryAverage || JohnAverage === MaryAverage){
    console.log('There is a draw');
}else if (JohnAverage>MikeAverage && JohnAverage>MaryAverage){
    console.log('John\'s average score is ' + JohnAverage +', he\'s the winner' );
}else{
    console.log('Mary\'s average score is ' + MaryAverage +', he\'s the winner' );
}




/*****************************
* Arrays
*/

// Initialize new array
var names = ['John', 'Mark', 'Jane'];
var years = new Array(1990, 1969, 1948);

console.log(names[2]);
console.log(names.length);

// Mutate array data
names[1] = 'Ben';
names[names.length] = 'Mary';
console.log(names);

// Different data types
var john = ['John', 'Smith', 1990, 'designer', false];

john.push('blue');
john.unshift('Mr.');
console.log(john);

john.pop();
john.pop();
john.shift();
console.log(john);

console.log(john.indexOf(23));

var isDesigner = john.indexOf('designer') === -1 ? 'John is NOT a designer' : 'John IS a designer';
console.log(isDesigner);


/*****************************
* CODING CHALLENGE 3
*/

/*
John and his family went on a holiday and went to 3 different restaurants. The bills were $124, $48 and $268.

To tip the waiter a fair amount, John created a simple tip calculator (as a function). He likes to tip 20% of the bill when the bill is less than $50, 15% when the bill is between $50 and $200, and 10% if the bill is more than $200.

In the end, John would like to have 2 arrays:
1) Containing all three tips (one for each bill)
2) Containing all three final paid amounts (bill + tip).

(NOTE: To calculate 20% of a value, simply multiply it with 20/100 = 0.2)

GOOD LUCK 😀
*/

function tipCalculator(bill){
    var percentage;
    if(bill<50){
        percentage = .2;
    }else if(bill>=50 && bill<200 ){
        percentage = .15;
    } else {
        percentage = .1;
    }
    return percentage * bill;
}
var bills = [124, 48, 268];
var tip = [
    tipCalculator(bills[0]),
    tipCalculator(bills[1]),
    tipCalculator(bills[2])
]

var finalAmount = [
    bills[0]+tip[0],
    bills[1]+tip[1],
    bills[2]+tip[2],
]

console.log('tips are '+tip);
console.log('final amount are '+finalAmount);

/*****************************
* Objects and properties
*/

// Object literal
var john = {
    firstName: 'John',
    lastName: 'Smith',
    birthYear: 1990,
    family: ['Jane', 'Mark', 'Bob', 'Emily'],
    job: 'teacher',
    isMarried: false
};

console.log(john.firstName);
console.log(john['lastName']);
var x = 'birthYear';
console.log(john[x]);

john.job = 'designer';
john['isMarried'] = true;
console.log(john);

// new Object syntax
var jane = new Object();
jane.firstName = 'Jane';
jane.birthYear = 1969;
jane['lastName'] = 'Smith';
console.log(jane);


/*****************************
* Objects and properties
*/

// Object literal
var john = {
    firstName: 'John',
    lastName: 'Smith',
    birthYear: 1990,
    family: ['Jane', 'Mark', 'Bob', 'Emily'],
    job: 'teacher',
    isMarried: false
};

console.log(john.firstName);
console.log(john['lastName']);
var x = 'birthYear';
console.log(john[x]);

john.job = 'designer';
john['isMarried'] = true;
console.log(john);

// new Object syntax
var jane = new Object();
jane.firstName = 'Jane';
jane.birthYear = 1969;
jane['lastName'] = 'Smith';
console.log(jane);

/*****************************
* Objects and methods
*/

var john = {
    firstName: 'John',
    lastName: 'Smith',
    birthYear: 1992,
    family: ['Jane', 'Mark', 'Bob', 'Emily'],
    job: 'teacher',
    isMarried: false,
    calcAge: function() {
        this.age = 2018 - this.birthYear;
    }
};

john.calcAge();
console.log(john);



/*****************************
* CODING CHALLENGE 4
*/

/*
Let's remember the first coding challenge where Mark and John compared their BMIs. Let's now implement the same functionality with objects and methods.
1. For each of them, create an object with properties for their full name, mass, and height
2. Then, add a method to each object to calculate the BMI. Save the BMI to the object and also return it from the method.
3. In the end, log to the console who has the highest BMI, together with the full name and the respective BMI. Don't forget they might have the same BMI.

Remember: BMI = mass / height^2 = mass / (height * height). (mass in kg and height in meter).

GOOD LUCK 😀
*/
var mark = {
    name : 'Mark',
    mass : 78,
    height : 1.69,
    calcBMI: function (){
        this.BMI =  this.mass/(this.height*this.height);
        return this.BMI;
    }
}
var john = {
    name : 'John',
    mass : 90,
    height : 1.72,
    calcBMI: function (){
        this.BMI =  this.mass/(this.height*this.height);
        return this.BMI;
    }
}
mark.calcBMI();
john.calcBMI();

console.log(mark,john);

if(mark.BMI > john.BMI){
    console.log(mark.name+' \'s BMI is higher,it\'s '+mark.BMI)
}else if(john.BMI>mark.BMI){
    console.log(john.name+' \'s BMI is higher,it\'s '+john.BMI)
}else{
    console.log('their BMI is the same')
    
}

// continue and break statements
var john = ['John', 'Smith', 1990, 'designer', false, 'blue'];

for (var i = 0; i < john.length; i++) {
    if (typeof john[i] !== 'string') continue;
    console.log(john[i]);
}

for (var i = 0; i < john.length; i++) {
    if (typeof john[i] !== 'string') break;
    console.log(john[i]);
}

// Looping backwards
for (var i = john.length-1 ; i >= 0; i--) {
    console.log(john[i]);
}

/*****************************
* CODING CHALLENGE 5
*/

/*
Remember the tip calculator challenge? Let's create a more advanced version using everything we learned!

This time, John and his family went to 5 different restaurants. The bills were $124, $48, $268, $180 and $42.
John likes to tip 20% of the bill when the bill is less than $50, 15% when the bill is between $50 and $200, and 10% if the bill is more than $200.

Implement a tip calculator using objects and loops:
1. Create an object with an array for the bill values
2. Add a method to calculate the tip
3. This method should include a loop to iterate over all the paid bills and do the tip calculations
4. As an output, create 1) a new array containing all tips, and 2) an array containing final paid amounts (bill + tip). HINT: Start with two empty arrays [] as properties and then fill them up in the loop.


EXTRA AFTER FINISHING: Mark's family also went on a holiday, going to 4 different restaurants. The bills were $77, $375, $110, and $45.
Mark likes to tip 20% of the bill when the bill is less than $100, 10% when the bill is between $100 and $300, and 25% if the bill is more than $300 (different than John).

5. Implement the same functionality as before, this time using Mark's tipping rules
6. Create a function (not a method) to calculate the average of a given array of tips. HINT: Loop over the array, and in each iteration store the current sum in a variable (starting from 0). After you have the sum of the array, divide it by the number of elements in it (that's how you calculate the average)
7. Calculate the average tip for each family
8. Log to the console which family paid the highest tips on average

GOOD LUCK 😀
*/

var john = {
    fullName:'John Smith',
    bills:[124, 48, 268, 180, 42],
    calcTips: function (){
        this.tips = [];
        this.finalValues = [];
        for(var i = 0; i < this.bills.length ; i++){
            var percentage;
            var bill = this.bills[i] ;
            if(this.bill > 200){
                percentage = .1
            }else if (this.bill>=50 && this.bill<200){
                percentage = .15
            }else{
                percentage = .2
            }

            this.tips[i] = bill * percentage;
            this.finalValues[i] = bill + bill * percentage;
        }
    }

}
var mark = {
    fullName:'Mark Miller',
    bills:[77, 475, 110, 45],
    calcTips: function (){
        this.tips = [];
        this.finalValues = [];
        for(var i = 0; i < this.bills.length ; i++){
            var percentage;
            var bill = this.bills[i] ;
            if(this.bill > 200){
                percentage = .1
            }else if (this.bill>=50 && this.bill<200){
                percentage = .15
            }else{
                percentage = .2
            }

            this.tips[i] = bill * percentage;
            this.finalValues[i] = bill + bill * percentage;
        }
    }

}


function calcAverage(tips){
    var sum = 0;
    for(var i = 0; i <tips.length; i++){
        sum = sum + tips[i];
    }
    return sum /tips.length;
}

john.calcTips();
mark.calcTips();

john.average = calcAverage(john.tips);
mark.average = calcAverage(mark.tips);

console.log(john);
console.log(mark);

if(john.average > mark.average){
    console.log(john.fullName + '\'s family pays higher tips, with an average of $' + john.average);
}else{
    console.log(mark.fullName + '\'s family pays higher tips, with an average of $' + mark.average);
}