const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry:['@babel/polyfill','./src/js/index.js'],   ///切入點
    output: {
        path:path.resolve(__dirname, 'dist'),
        filename:'js/bundle.js',
    },
    // mode:'development'   ///可以寫在package.json 中script裡
    devServer:{
        contentBase: './dist'
    },
    plugins:[
        new HtmlWebpackPlugin({
            filename:'index.html',
            template:'./src/index.html'
        })
    ],
    module:{
        rules:[{
            test:/\.js$/,
            exclude:/node_modules/,
            use:{
                loader:'babel-loader'
            }
        }]
    }
}

