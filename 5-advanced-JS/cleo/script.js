var john = {
  name: "John",
  yearOfBirth: 1990,
  job: "teacher"
};

var Person = function(name, yearOfBirth, job) {
  this.name = name;
  this.yearOfBirth = yearOfBirth;
  this.job = job;
  // this.calculateAge = function() {
  //     console.log(2016 - this.yearOfBirth)
  // }
};

Person.prototype.calculateAge = function() {
  console.log(2016 - this.yearOfBirth);
};

Person.prototype.lastName = "Smith";
var john = new Person("John", 1990, "teacher");
var jane = new Person("Jane", 1969, "designer");
var mark = new Person("Mark", 1948, "retired");
john.calculateAge();
jane.calculateAge();
mark.calculateAge();

console.log(john.lastName);
console.log(jane.lastName);
console.log(mark.lastName);

// Object.create

var personProto = {
  calculateAge: function() {
    console.log(2016 - yearOfBirth);
  }
};

var john = Object.create(personProto);
(john.name = "John"), (john.yearOfBirth = 1990), (john.job = "teacher");

var jane = Object.create(personProto, {
  name: { value: "Jane" },
  yearOfBirth: { value: 1969 },
  job: { value: "designer" }
});

//////////////////////////////
// Primitives vs object

// Primitives
var a = 23;
var b = a;
a = 46;
console.log(a); //46
console.log(b); //23

// Objects

var obj1 = {
  name: "John",
  age: 26
};

var obj2 = obj1;
obj1.age = 30;

console.log(obj1.age); //30
console.log(obj2.age); //30

// function

var age = 27;
var obj = {
  name: "Jonas",
  city: "Lisbon"
};

function change(a, b) {
  a = 30;
  b.city = "San Francisco";
}

change(age, obj);
console.log(age); //27
console.log(obj.city); ////San Francisco

/////////////////////////////
// Lecture: Passing functions as arguments

var year = [1990, 1965, 1937, 2005, 1998];

function arrayCalc(arr, fn) {
  var arrRes = [];
  for (var i = 0; i < arr.length; i++) {
    arrRes.push(fn(arr[i]));
  }
  return arrRes;
}

function calculateAge(el) {
  return 2016 - el;
}

function isFullAge(el) {
  return el >= 18;
}

function maxHeartRate(el) {
  if (el >= 18 && el <= 81) {
    return Math.round(206.9 - 0.67 * el);
  } else {
    return -1;
  }
}

var ages = arrayCalc(year, calculateAge);

var fullAges = arrayCalc(ages, isFullAge);
var rates = arrayCalc(ages, maxHeartRate);
console.log(ages, fullAges, rates);

// /////////////////////////////////////////
// Lecture: Functions returning functions

function interviewQuestion(job) {
  if (job === "designer") {
    return function(name) {
      console.log(name + ",can you please explain what ux design is");
    };
  } else if (job === "teacher") {
    return function(name) {
      console.log("what subjecy do you teach," + name);
    };
  } else {
    return function(name) {
      console.log("Hello!" + name, "what do you do?");
    };
  }
}

var teacherQuestion = interviewQuestion("teacher");
var designerQuestion = interviewQuestion("designer");

teacherQuestion("mary"); //what subjecy do you teach,mary
designerQuestion("john"); //John,can you please explain what ux design is
interviewQuestion("cop")("mark"); //Hello!mark what do you do?

/////////////////////////////
// Lecture: IIFE

(function() {
  var score = Math.random() * 10;
  console.log(score, score >= 5);
})();

(function(goodLuck) {
  var score = Math.random() * 10;
  console.log(score, score >= 5 - goodLuck);
})(5);

/////////////////////////////
// Lecture: Closures

function retirement(retirementAge) {
  var a = "years left until retirement.";
  return function(yearOfBirth) {
    var age = 2016 - yearOfBirth;
    console.log(retirementAge - age + a);
  };
}

var retirementUS = retirement(66);
retirementUS(1990);
retirement(66)(1990); //等同於上面兩行寫法

var retirementGermany = retirement(65);
retirementGermany(1990);

var retirementIceland = retirement(67);
retirementIceland(1990);

function interviewQuestion2(job) {
  return function(name) {
    if (job === "designer") {
      console.log(name + ",can you please explain what ux design is");
    } else if (job === "teacher") {
      console.log("what subjecy do you teach," + name);
    } else {
      console.log("Hello!" + name, "what do you do?");
    }
  };
}
interviewQuestion2("teacher")("Ariel");
interviewQuestion2("designer")("Joe");
interviewQuestion2("dancer")("Cleo");

/////////////////////////////
// Lecture: Bind, call and apply

var john = {
  name: "John",
  age: 26,
  job: "teacher",
  presentation: function(style, timeOfDay) {
    if (style === "formal") {
      console.log(
        "Good " +
          timeOfDay +
          ", Ladies and gentlement! I'm " +
          this.name +
          " I'm a " +
          this.job +
          " and I'm " +
          this.age +
          " years old!"
      );
    } else if (style === "friendly") {
      console.log(
        "Hey! What's up I'm " +
          this.name +
          " I'm a " +
          this.job +
          " and I'm " +
          this.age +
          " years old! Have a nice " +
          timeOfDay +
          "."
      );
    }
  }
};

var emily = {
  name: "Emily",
  age: 24,
  job: "designer"
};

john.presentation("friendly", "afternoon");

// call function

john.presentation.call(emily, "friendly", "afternoon");

// apply function 但此案例不能用

john.presentation.apply(emily, ["friendly", "afternoon"]);

// bind function

var johnFormal = john.presentation.bind(john, "formal", "evening");
johnFormal();

var emilyFormal = john.presentation.bind(john, "formal");
emilyFormal("night");

var year = [1990, 1965, 1937, 2005, 1998];

function arrayCalc(arr, fn) {
  var arrRes = [];
  for (var i = 0; i < arr.length; i++) {
    arrRes.push(fn(arr[i]));
  }
  return arrRes;
}

function calculateAge(el) {
  return 2016 - el;
}

function isFullAge(limit, el) {
  return el >= limit;
}

var ages = arrayCalc(year, calculateAge);
console.log(ages);
var fullJapan = arrayCalc(ages, isFullAge.bind(this, 20));

console.log(fullJapan);

/////////////////////////////
// CODING CHALLENGE

/*
--- Let's build a fun quiz game in the console! ---

1. Build a function constructor called Question to describe a question. A question should include:
a) question itself
b) the answers from which the player can choose the correct one (choose an adequate data structure here, array, object, etc.)
c) correct answer (I would use a number for this)

2. Create a couple of questions using the constructor

3. Store them all inside an array

4. Select one random question and log it on the console, together with the possible answers (each question should have a number) (Hint: write a method for the Question objects for this task).

5. Use the 'prompt' function to ask the user for the correct answer. The user should input the number of the correct answer such as you displayed it on Task 4.

6. Check if the answer is correct and print to the console whether the answer is correct ot nor (Hint: write another method for this).

7. Suppose this code would be a plugin for other programmers to use in their code. So make sure that all your code is private and doesn't interfere with the other programmers code (Hint: we learned a special technique to do exactly that).
*/

(function() {
  function Question(question, answers, correct) {
    this.question = question;
    this.answers = answers;
    this.correct = correct;
  }

  Question.prototype.displayQuestion = function() {
    console.log(this.question);
    for (var i = 0; i < this.answers.length; i++) {
      console.log(i + ": " + this.answers[i]);
    }
  };

  Question.prototype.checkAnswer = function(ans, callback) {
    var sc;
    if (ans === this.correct) {
      console.log("Correct answer!");
      sc = callback(true);
    } else {
      console.log("Wrong answer. Try again :)");
      sc = callback(false);
    }

    this.displayScore(sc);
  };

  Question.prototype.displayScore = function(score) {
    console.log("Your current score is: " + score);
    console.log("--------------------");
  };

  var q1 = new Question(
    "Is JavaScript the coolest programming language in the world?",
    ["Yes", "No"],
    0
  );
  var q2 = new Question(
    "What is the name of this course's teacher?",
    ["John", "Micheal", "Jonas"],
    2
  );
  var q3 = new Question(
    "What does best describe coding?",
    ["Boring", "Hard", "Fun", "Tediuos"],
    2
  );

  var questions = [q1, q2, q3];

  function score() {
    var sc = 0;
    return function(correct) {
      if (correct) {
        sc++;
      }
      return sc;
    };
  }

  var keepScore = score();

  function nextQuestion() {
    var n = Math.floor(Math.random() * questions.length);
    questions[n].displayQuestion();
    var answer = prompt("please select the answer.");
    if (answer !== 'exit') {
      questions[n].checkAnswer(parseInt(answer), keepScore);
      nextQuestion();
    }
  }

  nextQuestion();

})();
