// Global app controller

/*

// WAY 1
import str from './models/Search';

// WAY 2
// import {add as a, multiply as m, ID} from './views/searchView';
 
// WAY 3 
import * as searchView from './views/searchView';

// console.log(`Using imported function! ${a(ID, 2)} and ${m(2,3)} , ${str}`);
console.log(`Using imported function! ${searchView.add(searchView.ID, 2)} and ${searchView.multiply(2,3)} , ${str}`); 

*/
// https://www.food2fork.com/api/search

// a23e378adca44b74bc72c2e9d17a410f

import Search from "./models/Search";
import Recipe from "./models/Recipe";
import * as searchView from "./views/searchView";
import * as recipeView from "./views/recipeView";
import { elements } from "./views/base";

/*
 * Global state of the app
 * - Search object
 * - Current recipe object
 * - Shopping list object
 * - Liked recipes
 */

const state = {};

/*
 **SEARCH CONTROLLER
 */
const controlSearch = async () => {
  // 1) Get query from view
  // const query = searchView.getInput();
  const query = "pizza";

  if (query) {
    // 2) New search object and add to state
    state.search = new Search(query);
    // 3) Prepare UI for results
    searchView.clearInput();
    searchView.clearResults();
    // 4) Search for recipes
    await state.search.getResults();

    // 5) Render results on UI
    searchView.renderResults(state.search.result);
  }
};

elements.searchForm.addEventListener("submit", e => {
  e.preventDefault();
  controlSearch();
});

//TESTING
window.addEventListener("load", e => {
  e.preventDefault();
  controlSearch();
});

elements.searchResPages.addEventListener("click", e => {
  // 為了不選取到部更細節的元素
  const btn = e.target.closest(".btn-inline");
  // console.log(btn);

  if (btn) {
    const goToPage = parseInt(btn.dataset.goto, 10);
    searchView.clearResults();
    searchView.renderResults(state.search.result, goToPage);
    console.log(goToPage);
  }
});

/*
 **RECIPE CONTROLLER
 */

const controlRecipe = async () => {
  //get ID from url
  const id = window.location.hash.replace("#", "");
  console.log(id);

  if (id) {
    //prepare UI for changes

    // Create new recipe object
    state.recipe = new Recipe(id);

    //TESTING
    window.r = state.recipe;
    try {
      // Get recipe  data
      await state.recipe.getRecipe();

      // Calculate servings and TimeRanges;
      state.recipe.calcTime();
      state.recipe.calcServings();

      // Render recipe
      console.log(state.recipe);
    } catch (err) {
      alert("Error processing recipe!");
    }
  }
};

/********************

// hashChange 指連結#之後改變
 window.addEventListener('hashchange',controlRecipe);
 //一載入頁面時
 window.addEventListener('load',controlRecipe); 

 ******************/

/****************************************************** 
  
    上面兩行改寫為一行 如果有10行會相當好用  
    
 *********************************************/

["hashchange", "load"].forEach(event =>
  window.addEventListener(event, controlRecipe)
);
